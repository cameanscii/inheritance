package pl.sda.auta;

public class Kabriolet extends Samochod {
    public boolean dachSchowany = false;

    public Kabriolet(String kolor, String marka, int rocznik) {
        super(kolor, marka, rocznik);
    }

    public void schowajDach() {
        dachSchowany = true;
    }

    public boolean czyDachSchowany() {
        return dachSchowany;
    }

    @Override
    public void przyspiesz() {
        if (predkosc >= 180) {
            System.out.println("\nSamochód nie może jechać więcej niż 180km/h");

        } else {
            predkosc += 40;
            if (predkosc >= 180) {
                System.out.println("\nSamochód nie może jechać więcej niż 180km/h");
            }
            else{System.out.printf("\nPrzyspieszam do %d km/h", predkosc);}
        }
    }

//    @Override
//    public String toString() {
//        String stringer = String.format("%s samochód  marki %s rocznik %d z rozsuwanym dachem",kolor,marka,rocznik);
//        return stringer;
//    }

    @Override
    public String toString() {
        return super.toString()+" z rozsuwanym dachem";
    }
}
