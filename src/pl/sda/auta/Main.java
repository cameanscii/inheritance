package pl.sda.auta;

public class Main {
    public static void main(String[] args) {
        Samochod bmw = new Samochod("zielony","bmw",1990);
        bmw.przyspiesz();
        System.out.println("\n" + bmw.czyWlaczoneSwiatla);

        Kabriolet speeder = new Kabriolet("zielony","bmw",1990);
        speeder.wlaczSwiatla();
        speeder.schowajDach();
        System.out.println(speeder.czyDachSchowany());
        System.out.println(speeder.czyWlaczoneSwiatla);
        for (int i = 0; i < 6 ; i++) {
            speeder.przyspiesz();
        }

        System.out.println(bmw.equals(speeder));

        System.out.println(bmw.toString());
        System.out.println(speeder.toString());

    }
}
