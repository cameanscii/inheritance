package pl.sda.figury;

public class Kolo {
    double promien;
    protected final double LICZBA_PI = 3.14;



    public Kolo(double promien) {
        this.promien = promien;
    }

    public final double obliczPole(){
        double pole = LICZBA_PI*promien*promien;
        return pole;
    }


}
