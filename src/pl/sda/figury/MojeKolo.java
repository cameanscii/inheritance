package pl.sda.figury;

public class MojeKolo extends Kolo {
    //6.4 nadpisanie-mozliwe oraz 7.0 dodanie final dodane przed argumentem  powoduje ze konstruktor nie ma do
//    czego sie odwolac, nie moze dziedziczyc
    public MojeKolo(double promien) {
         super (promien);}
//        6.1 nadpisanie wart PI
//        LICZBA_PI = 5; cannot assign value to a final variable

//        6.2   nadpisanie metody obliczPole - nie da sie nie mamy do wyboru po kliknieciu ctrl+o oraz wyskakuje blad
//            cannot override
//        @Override
//        public final double obliczPole(){
//            double pole2 = LICZBA_PI * promien * promien;
//            return pole2;
//        }

        //6.3 mozemy przeciazyc poprzez wpisanie w nawias argumentu
        public final double obliczPole(double a){
            double pole = LICZBA_PI*promien*promien;
            return pole;
        }

    }



