package pl.sda.osoby;

public class Main {
    public static void main(String[] args) {
        Osoba roman = new Osoba("Roman","Kowalski",25);
        roman.przedstawSie();

        Student olek = new Student("Aleksander","Nowak",21, 111);
        olek.przedstawSie();


        //sout ma domyslna metode toString() czyli ponizej jest zawarte olek.toString() choc nie widac
        System.out.println(olek);

        Student bolek = new Student("Boleslaw", "Zieliński", 35, 111);
        System.out.println(olek.equals(bolek));
        System.out.println(olek.equals("Bolek"));
    }
}
